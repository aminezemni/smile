//
//  HomeViewController.swift
//  smile
//
//  Created by zemni on 10/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import Foundation
import UIKit
import Floaty
import Tabman
import Pageboy

class HomeViewController : TabmanViewController {
    
    var floaty = Floaty()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        
        // configure the bar
        self.bar.items = [Item(title: "Actu"),
                          Item(title: "Objects"),
                          Item(title: "Services"),
                          Item(title: "Requests"),
                          Item(title: "Map")]
        
        self.bar.style = .scrollingButtonBar
        self.createAddFAB()
        self.createSearchBar()
    }
    
    func createSearchBar(){
        let searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.placeholder = "Hammer, carpooling, babysitting..."
        
        self.navigationItem.titleView = searchBar
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createAddFAB() {
        
        floaty.hasShadow = false
        floaty.plusColor = UIColor.smile.yellow
        floaty.buttonColor = UIColor.smile.blue
        floaty.itemTitleColor = UIColor.white
        floaty.itemButtonColor = UIColor.smile.blue
        floaty.addItem("Search service", icon: UIImage(named: "paintbrush"))
        floaty.addItem("Propose service", icon: UIImage(named: "paintbrush"))
        floaty.addItem("Propose carpooling", icon: UIImage(named: "paintbrush"))
        floaty.addItem("Inform neighbors", icon: UIImage(named: "paintbrush"))
        floaty.addItem("Search object", icon: UIImage(named: "paintbrush"))
        floaty.addItem("Report town hall", icon: UIImage(named: "paintbrush"))
        
        
        self.view.addSubview(floaty)
        
    }
}

extension HomeViewController : UISearchBarDelegate {
    
    
}

extension HomeViewController : PageboyViewControllerDataSource {
    
    func viewControllers(forPageboyViewController pageboyViewController: PageboyViewController) -> [UIViewController]? {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let ObjectViewC = storyboard.instantiateViewController(withIdentifier: "ObjectViewController") as! ObjectViewController
        let ObjectViewController = UINavigationController(rootViewController: ObjectViewC)
        ObjectViewC.home = self
        
        let mapViewC = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        let mapViewController = UINavigationController(rootViewController: mapViewC)

        
   
        return [ObjectViewController,ObjectViewController,ObjectViewController,ObjectViewController, mapViewController]
    }
    
    func defaultPageIndex(forPageboyViewController pageboyViewController: PageboyViewController) -> PageboyViewController.PageIndex? {
        return PageIndex.first
    }
    
    
}





