//
//  ProfilViewController.swift
//  smile
//
//  Created by zemni on 10/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import Foundation
import UIKit

class ProfilViewController : UIViewController {
    
    @IBOutlet weak var profilPicture: UIImageView!
    @IBOutlet weak var profilName: UILabel!
    @IBOutlet weak var profilAdress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.initLayout()
    }
    
    func initLayout(){
        profilPicture.layer.cornerRadius = profilPicture.layer.bounds.width / 2
    }

    
}
