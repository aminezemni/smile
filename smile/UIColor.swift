//
//  UIColor.swift
//  smile
//
//  Created by zemni on 10/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    struct smile{
        
        static let blue = UIColor.init(netHex: 0x139CD0)
        static let yellow = UIColor.init(netHex: 0xFECE0A)
    }
    
}
