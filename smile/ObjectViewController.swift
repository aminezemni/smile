//
//  ActuViewController.swift
//  smile
//
//  Created by zemni on 10/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class ObjectViewController : UITableViewController {
    
    var objects : [Object]!
    var home : HomeViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsetsMake(44, 0, 0, 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ObjectWebService().getObject(completion: { (objs) in
            self.objects = objs
            self.tableView.reloadData()
        })
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "objectCell", for: indexPath) as! ObjectTableViewCell
        
        for obj in objects {
            let objectVM = ObjectViewModel.init(obj: obj)
            cell.typeLabel.text = objectVM.title
            cell.announcelabel.text = objectVM.typeAndPrice
            cell.subjectLabel.text = objectVM.object
            cell.dateAndDistanceLabel.text = objectVM.dateAndDistance
            cell.nameLabel.text = objectVM.seller.name
            let url = URL(string: objectVM.image)
            cell.cellImage.kf.setImage(with: url)
        }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        home.performSegue(withIdentifier: "showObjectDetail", sender: self)
    }
    
    
}
