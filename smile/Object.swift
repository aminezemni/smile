//
//  Actu.swift
//  smile
//
//  Created by zemni on 10/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import Foundation
import JASON

struct Object {
    
    var image : String
    var type : String
    var object : String
    var price : Int
    var rentOrSell : Bool
    var seller : User
    var date : Date
    var distance : Int
    
    init(image : String, type : String, object : String, price : Int, ros : Bool, seller : User, date : Date, distance : Int) {
        self.image = image
        self.type = type
        self.object = object
        self.price = price
        self.rentOrSell = ros
        self.date = date
        self.seller = seller
        self.distance = distance
    }
    
    init(_ json: JSON) {
        
        JSON.dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        image = json["image"].string!
        type = json["type"].string!
        
        object = json["object"].string!
        price  = json["price"].int!
        
        rentOrSell = json["rentOrSell"].bool!
        
        seller = User(json["seller"])
        date = json["date"].nsDate!
        distance = json["distance"].int!
    }
}
