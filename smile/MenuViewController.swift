//
//  MenuViewController.swift
//  smile
//
//  Created by zemni on 09/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import Foundation
import UIKit

enum Menu: Int {
    case profil = 0
    case home
}

class MenuViewController : UITableViewController {
    
    @IBOutlet weak var profilPicture: UIImageView!
    @IBOutlet weak var profilName: UILabel!
    
    var mainViewController: UIViewController!
    var profilViewController: UIViewController!
    var homeViewController: UIViewController!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let profilViewC = storyboard.instantiateViewController(withIdentifier: "ProfilViewController") as! ProfilViewController
        profilViewController = UINavigationController(rootViewController: profilViewC)
        
        let homeViewC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        homeViewController = UINavigationController(rootViewController: homeViewC)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initLayout()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let menu = Menu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func changeViewController(_ menu: Menu) {
        switch menu {
            
            case .profil:
                self.slideMenuController()?.changeMainViewController(self.profilViewController, close: true)
            
            case .home:
                self.slideMenuController()?.changeMainViewController(self.homeViewController, close: true)
        
        }
    }
    
    func initLayout() {
        profilPicture.layer.cornerRadius = profilPicture.layer.bounds.width / 2
        self.slideMenuController()?.changeMainViewController(self.homeViewController, close: true)
        
    }
}
