//
//  ObjectWebService.swift
//  smile
//
//  Created by zemni on 10/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import Foundation
import JASON
//import Alamofire

class ObjectWebService {
    
    func getObject(completion: @escaping ([Object]?) -> Void){
        
        //exemple of webservice
        /*Alamofire.request(.GET, objectURL).responseJASON { response in
         if let json = response.result.value {
                let objects = json.map(Objects.init)
         
            }
        }*/
        
        //fake data
        
        let dictionary : [String : Any]? = [
        "image": "https://cdn.manomano.fr/bosch-perceuse-percussion-850-w-gsb19-2re-060117b500-T-266736-907046_1.jpg",
        "type": "DIY and construction",
        "object": "Drill",
        "seller": [
            "name": "amine zemni",
            "adress": "50 rue de paris",
            "picture": "https://cdn.manomano.fr/bosch-perceuse-percussion-850-w-gsb19-2re-060117b500-T-266736-907046_1.jpg"
        ],
        "rentOrSell": true,
        "price" : 20,
        "date": "2018-05-23T18:25:43",
        "distance": 150]
        
        if dictionary != nil {
            
            let object = Object(JSON(dictionary))
            var objects : [Object] = []
            for _ in 1...5 {
                objects.append(object)
            }
            
            completion(objects)
            
        } else {
            completion(nil)
        }
        
        
        
    }
}



