//
//  ViewController.swift
//  smile
//
//  Created by zemni on 09/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UISearchBarDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

