//
//  User.swift
//  smile
//
//  Created by zemni on 10/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import Foundation
import JASON

struct User {
    
    let name : String
    let picture : String
    let adress : String
    
    init(name : String, picture : String, adress : String) {
        self.name = name
        self.picture = picture
        self.adress = adress
    }
    
    init(_ json: JSON) {
        
        name = json["name"].string!
        picture = json["picture"].string!
        adress = json["adress"].string!
    }
}
