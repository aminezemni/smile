//
//  ActuTableViewCell.swift
//  smile
//
//  Created by zemni on 10/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import Foundation
import UIKit

class ObjectTableViewCell : UITableViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var announcelabel: UILabel!
    @IBOutlet weak var dateAndDistanceLabel: UILabel!
    @IBOutlet weak var pictureBackgroundView: UIView!
    @IBOutlet weak var pictureProfil: UIImageView!
    @IBOutlet weak var nameBackgroundView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        
    }
    
    override func awakeFromNib() {
        
        pictureBackgroundView.layer.cornerRadius = pictureBackgroundView.layer.bounds.width / 2
        pictureProfil.layer.cornerRadius = pictureProfil.layer.bounds.width / 2
        nameBackgroundView.layer.cornerRadius = 4
        backView.layer.cornerRadius = 2
        let path = UIBezierPath(roundedRect:cellImage.bounds,
                                byRoundingCorners:[.topLeft, .bottomLeft],
                                cornerRadii: CGSize(width: 2, height:  2))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        cellImage.layer.mask = maskLayer
        
        let shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: backView.bounds.width + 40, height: backView.bounds.height ), cornerRadius: 2)
        backView.layer.shadowColor = UIColor.black.cgColor
        backView.layer.masksToBounds = false
        backView.layer.shadowOpacity = 0.5
        backView.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        backView.layer.shadowPath = shadowPath.cgPath
        
    }
    
}
