//
//  ObjectViewModel.swift
//  smile
//
//  Created by zemni on 10/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import Foundation
import UIKit

struct ObjectViewModel {
    
    let title : String
    let object : String
    let typeAndPrice : String
    let dateAndDistance : String
    let image : String
    let seller : User
    
    init(obj : Object) {
        
        title = obj.type
        object = obj.object
        typeAndPrice = (obj.rentOrSell == true) ? "Object for Rent \(obj.price)/day"  : "Object for sale - \(obj.price)"
        
        let calendar = NSCalendar.current
        
        let date1 = calendar.startOfDay(for: obj.date)
        let date2 = calendar.startOfDay(for: Date())
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        dateAndDistance = ( (components.day! > 31) ? "\(components.month!) months ago" : "\(components.day!) day ago" ) + " | about \(obj.distance) M"
        image = obj.image
        seller = obj.seller
    }
    
}
