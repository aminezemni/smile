//
//  smileTests.swift
//  smileTests
//
//  Created by zemni on 12/06/2018.
//  Copyright © 2018 zemni. All rights reserved.
//

import XCTest
@testable import smile

class smileObjectTests: XCTestCase {
    
    var ObjectWebServiceTest : ObjectWebService!
    var objectForTest : Object!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        ObjectWebServiceTest = ObjectWebService()
        let user = User(name : "amine", picture : "url2", adress : "paris" )
        objectForTest = Object(image : "url",
                               type : "bricolage",
                               object : "perceuse",
                               price : 10,
                               ros : true,
                               seller : user,
                               date : Date().addingTimeInterval(-100000),
                               distance : 150)
        
    }
    
    func testGetObject() {
        
        ObjectWebServiceTest.getObject { (obj) in
            XCTAssertNotNil(obj)
        
        }
    }
    
    func testObjectIntegrity() {
        
        ObjectWebServiceTest.getObject { (obj) in
            XCTAssertNotNil(obj)
            XCTAssertNotNil(obj?[1])
            XCTAssertNotNil(obj?[1].date)
            XCTAssertNotNil(obj?[1].distance)
            XCTAssertNotNil(obj?[1].image)
            XCTAssertNotNil(obj?[1].object)
            XCTAssertNotNil(obj?[1].price)
            XCTAssertNotNil(obj?[1].rentOrSell)
            XCTAssertNotNil(obj?[1].seller)
            XCTAssertNotNil(obj?[1].type)
        }
    }

    func testObjectVMInit() {
        
        let objectVM = ObjectViewModel(obj: objectForTest)
        
        XCTAssertEqual(objectVM.image, "url")
        XCTAssertEqual(objectVM.object, "perceuse")
        XCTAssertEqual(objectVM.title, "bricolage")
        XCTAssertEqual(objectVM.typeAndPrice, "Object for Rent 10/day")
        XCTAssertEqual(objectVM.dateAndDistance, "2 day ago | about 150 M")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        ObjectWebServiceTest = nil
        objectForTest = nil
        super.tearDown()
    }
    
}
